import psycopg2
import pandas as pd

# DB적재시 오류를 유발하는 DECIMAL을 제거
def remove_decimal(origin_dataset,save_path):

    object_columns = []
    find_object_col = origin_dataset.dtypes == object

    for obj in range(len(find_object_col)):
        if find_object_col[obj] == True:
            object_columns.append(origin_dataset.columns[obj])

    for col in range(len(object_columns)):
        origin_dataset[object_columns[col]] = origin_dataset[object_columns[col]].map(lambda x : x.replace(',',''))
    origin_dataset.to_csv(save_path, sep=',', header=False, index=False)

    return()

origin_dataset = pd.read_csv("../labeled_data.csv")
save_path = "../labeled_data_after_remove_decimal.csv"

remove_decimal(origin_dataset, save_path)

connect = None
cursor = None

try:
    connect = psycopg2.connect(host='192.168.0.***', port='****', dbname='mydatabase', user = 'postgres',password = '****')
    connect.autocommit = True
    cursor = connect.cursor()

    with open(r'../labeled_data_after_remove_decimal.csv_set','rt',encoding='UTF-8') as f:
        cursor.copy_from(file=f, table='public.temp_tableset_test',sep=',')
# mobile.T_RAW_MATERIAL_STORE

except Exception as e:
    print(e)

finally:
    if cursor:
        cursor.close()
    if connect:
        connect.close()
