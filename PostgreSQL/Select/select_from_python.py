import psycopg2
import pandas as pd
import copy


def set_data():
    connect = None
    cursor = None

    try:
        connect = psycopg2.connect(host='192.168.0.***', port='****', dbname='mydatabase', user='postgres',password='****')
        #connect.autocommit = True
        cursor = connect.cursor()

        SQL = 'SELECT * FROM temp_tableset_test ORDER BY timestamp;'

        cursor.execute(SQL)
        result = cursor.fetchall()

        temp_data_frame = pd.DataFrame(result)
        temp_data_frame.columns = [desc[0] for desc in cursor.description]

    except Exception as e:
        print(e)

    return temp_data_frame

# DB상에서 대문자,소문자는 구분이 없으며 모두 소문자로 인식함.
# DB에서 PYTHON으로 불러올 때 역시 소문자로 불러오게 됨, DB적재 전 데이터 분석이 우선 이뤄진경우 아래와 같이 COLUMN 명들을 CSV에 매칭되도록 변경하여 사용함.
# DB적재가 우선시 된다면,데이터 분석 시 COLUMN 명들은 소문자로 사용함.
rename =['_id', 'TimeStamp','PART_FACT_PLAN_DATE','PART_FACT_SERIAL','PART_NAME' ,'EQUIP_CD','EQUIP_NAME','PassOrFail',
'Reason','Injection_Time','Filling_Time','Plasticizing_Time','Cycle_Time','Clamp_Close_Time','Cushion_Position'
,'Switch_Over_Position','Plasticizing_Position','Clamp_Open_Position','Max_Injection_Speed','Max_Screw_RPM'
,'Average_Screw_RPM','Max_Injection_Pressure','Max_Switch_Over_Pressure','Max_Back_Pressure','Average_Back_Pressure'
,'Barrel_Temperature_1','Barrel_Temperature_2','Barrel_Temperature_3','Barrel_Temperature_4','Barrel_Temperature_5'
,'Barrel_Temperature_6','Barrel_Temperature_7','Hopper_Temperature'
,'Mold_Temperature_1','Mold_Temperature_2','Mold_Temperature_3','Mold_Temperature_4','Mold_Temperature_5'
,'Mold_Temperature_6','Mold_Temperature_7','Mold_Temperature_8','Mold_Temperature_9','Mold_Temperature_10'
,'Mold_Temperature_11','Mold_Temperature_12']

label_data_origin = set_data()
label_data_origin.columns = rename

label_data = copy.deepcopy(label_data_origin)

print(label_data.head(4))
