import psycopg2
from psycopg2.extensions import AsIs

connect = None
cursor = None

try:
    connect = psycopg2.connect(host='192.168.0.***', port='****', dbname='mydatabase', user = 'postgres',password = '****')
    connect.autocommit = True
    cursor = connect.cursor()

    sql = 'DROP TABLE IF EXISTS %(table)s'
    sql_parameter_dict = {'table':AsIs("public.TEMP_TABLESET_TEST")}
    cursor.execute(sql, sql_parameter_dict)

# DB에 적재할 CSV 파일과 동일한 Column으로 DB TABLE 생성
    sql = ""
    sql += "CREATE TABLE %(table)s ( "
    sql += "    _id                     character varying(30) NOT NULL, "
    sql += "    TimeStamp               character varying(30) NOT NULL, "
    sql += "    PART_FACT_PLAN_DATE     character varying(30) NOT NULL, "
    sql += "    PART_FACT_SERIAL        character varying(30) NOT NULL, "
    sql += "    PART_NAME               character varying(30) NOT NULL, "
    sql += "    EQUIP_CD                character varying(30) NOT NULL, "
    sql += "    EQUIP_NAME              character varying(30) NOT NULL, "
    sql += "    PassOrFail              character varying(30) NOT NULL, "
    sql += "    Reason                  character varying(30) NOT NULL, "
    sql += "    Injection_Time          character varying(30) NOT NULL, "
    sql += "    Filling_Time            character varying(30) NOT NULL, "
    sql += "    Plasticizing_Time       character varying(30) NOT NULL, "
    sql += "    Cycle_Time              character varying(30) NOT NULL, "
    sql += "    Clamp_Close_Time        character varying(30) NOT NULL, "
    sql += "    Cushion_Position        character varying(30) NOT NULL, "
    sql += "    Switch_Over_Position    character varying(30) NOT NULL, "
    sql += "    Plasticizing_Position   character varying(30) NOT NULL, "
    sql += "    Clamp_Open_Position     character varying(30) NOT NULL, "
    sql += "    Max_Injection_Speed     character varying(30) NOT NULL, "
    sql += "    Max_Screw_RPM           character varying(30) NOT NULL, "
    sql += "    Average_Screw_RPM       character varying(30) NOT NULL, "
    sql += "    Max_Injection_Pressure  character varying(30) NOT NULL, "
    sql += "    Max_Switch_Over_Pressure character varying(30)NOT NULL, "
    sql += "    Max_Back_Pressure       character varying(30) NOT NULL, "
    sql += "    Average_Back_Pressure   character varying(30) NOT NULL, "
    sql += "    Barrel_Temperature_1    character varying(30) NOT NULL, "
    sql += "    Barrel_Temperature_2    character varying(30) NOT NULL, "
    sql += "    Barrel_Temperature_3    character varying(30) NOT NULL, "
    sql += "    Barrel_Temperature_4    character varying(30) NOT NULL, "
    sql += "    Barrel_Temperature_5    character varying(30) NOT NULL, "
    sql += "    Barrel_Temperature_6    character varying(30) NOT NULL, "
    sql += "    Barrel_Temperature_7    character varying(30) NOT NULL, "
    sql += "    Hopper_Temperature      character varying(30) NOT NULL, "
    sql += "    Mold_Temperature_1      character varying(30) NOT NULL, "
    sql += "    Mold_Temperature_2      character varying(30) NOT NULL, "
    sql += "    Mold_Temperature_3      character varying(30) NOT NULL, "
    sql += "    Mold_Temperature_4      character varying(30) NOT NULL, "
    sql += "    Mold_Temperature_5      character varying(30) NOT NULL, "
    sql += "    Mold_Temperature_6      character varying(30) NOT NULL, "
    sql += "    Mold_Temperature_7      character varying(30) NOT NULL, "
    sql += "    Mold_Temperature_8      character varying(30) NOT NULL, "
    sql += "    Mold_Temperature_9      character varying(30) NOT NULL, "
    sql += "    Mold_Temperature_10     character varying(30) NOT NULL, "
    sql += "    Mold_Temperature_11     character varying(30) NOT NULL, "
    sql += "    Mold_Temperature_12     character varying(30) NOT NULL "
    sql += ") "

    cursor.execute(sql, sql_parameter_dict)

except Exception as e:
    print(e)

finally:
    if cursor:
        cursor.close()
    if connect:
        connect.close()
