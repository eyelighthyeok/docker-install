╠═1 [Docker & PostgreSQL Install](https://gitlab.com/eyelighthyeok/docker-install/-/blob/master/Docker%20Install%20On%20Window/READ_ME.MD)    
║░╠═1 [Using Docker-compose.yml](https://gitlab.com/eyelighthyeok/docker-install/-/blob/master/docker-compose.yml/docker-compose.yml)  
╠═2 PostgreSQL  
║░╠═1 Init  
║░░╚═1 [init_table](https://gitlab.com/eyelighthyeok/docker-install/-/blob/master/PostgreSQL/Init/init_table.py)   
║░╠═2 Insert  
║░░╚═1 [copy_from_csv](https://gitlab.com/eyelighthyeok/docker-install/-/blob/master/PostgreSQL/copy_from/copy_from_csv.py)  
║░╠═3 Select  
║░░╚═1 [select_from_python](https://gitlab.com/eyelighthyeok/docker-install/-/blob/master/PostgreSQL/Select/select_from_python.py)  
║░╠═4 Update  
╚═README.md  

( ║ ╠ ═ ╚ ░ )
